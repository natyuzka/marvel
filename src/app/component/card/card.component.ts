import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { SearchService } from '../../service/search.service';
import { FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator } from '@angular/material';
import { DialogAddCharacterComponent } from '../dialog-add-character/dialog-add-character.component';

export interface Order {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent implements OnInit {

  @Output() emitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  public form: FormGroup;
  public loading: boolean;
  public characters;
  public searchText: string;
  pageChange: number = 1;
  itemPerPage = 10;
  indexPage = 1;
  total = 0;
  dataPage = 0;
  selectOrder = "";
  orders: Order[] = [
    {viewValue: "Sort by", value: ""},
    {viewValue: "Name A-Z", value: "name"},
    {viewValue: "Name Z-A", value: "-name"},
    {viewValue: "Modified Present-Past", value: "modified"},
    {viewValue: "Modified Past-Present", value: "-modified"}
  ];

  constructor(private auth: AuthService, public dialog: MatDialog, private search: SearchService) {
    this.loading = true;
    this.form = new FormGroup({
      'selectOrder': new FormControl('')
    });
    this.form.valueChanges.subscribe((data: any) => {
      this.selectOrder = data.selectOrder;
      this.character();
    });
  }

  ngOnInit() {
    this.search.text.subscribe((text: string) => {
      this.searchText = text;
      this.character();
    })
    this.character();
  }

  character() {
      console.log(this.selectOrder);
      this.auth.getCharacters(this.searchText, this.selectOrder).subscribe((response: any) => {
      this.resultSearch(response);
      this.loading = false;
    }, (error: any) => {
      console.log(error);

    });
  }

  resultSearch(data) {
    this.characters = data.data.results;
    this.loading = false
  }

  getImage(thumbnail) {
    return `${thumbnail.path}.${thumbnail.extension}`;
  }

  openDialog(resourceURI): void {
    let dialogRef = this.dialog.open(DialogAddCharacterComponent, {
      width: '50%',
      data: { url: "https" + resourceURI.substr(4) }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed");
    })
  }

  pageChanged(event: any) {
    this.indexPage = event;
    this.dataPage = (+event - 1) * this.itemPerPage;
  }

}