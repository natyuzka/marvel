import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../../service/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import { AddFavouritesService } from '../../service/add-favourites.service';

export interface DialogData {
  url: string;
}

@Component({
  selector: 'app-dialog-add-character',
  templateUrl: './dialog-add-character.component.html',
  styleUrls: ['./dialog-add-character.component.css']
})
export class DialogAddCharacterComponent implements OnInit {

  public form: FormGroup;

  public loading: boolean;
  public comic;
  favourite = [];
  constructor(public add: AddFavouritesService,
    private auth: AuthService,
    public dialogRef: MatDialogRef<DialogAddCharacterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.loading = true;
    this.form = new FormGroup({
      'button': new FormControl(false)
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.auth.getComicId(this.data.url).subscribe((response: any) => {
      this.comic = response.data.results[0];
      console.log(this.comic);
      
      this.loading = false;
    }, (error: any) => {
      console.log(error);

    });
  }
  getImage(thumbnail) {
    return `${thumbnail.path}.${thumbnail.extension}`;
  }

  saveComic(idComic, thumbnail, title) {

    this.favourite = JSON.parse(localStorage.getItem("comic"));
    let findId = false;
    if (this.favourite) {
      this.favourite.forEach(element => {
        if (element.id == idComic) {
          findId = true
        }
      });
    } else {
      this.favourite = [];
    }

    if (findId == false && this.favourite.length <= 2) {
      let data = {
        id: idComic, img: thumbnail, title: title
      }
      this.favourite.push(data);

      localStorage.setItem("comic", JSON.stringify(this.favourite));
      alert("¡Favorito agregado!");
    } else {
      if (findId != false)
        alert("Este comic ya esta en tus favoritos!");
      else
        alert("No puedes agregar mas favoritos!");
    }
    console.log(this.form.value);
    
    this.add.addFavourites(this.form.value);
  }
}
