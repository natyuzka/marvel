import { ChangeDetectionStrategy, Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AddFavouritesService } from '../../service/add-favourites.service';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  @Output() emitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  comics;

  constructor(public add: AddFavouritesService) {
  }


  ngOnInit() {
    this.add.text.subscribe((text: string) => {
      this.comics = JSON.parse(localStorage.getItem("comic"));
    });
    this.comics = JSON.parse(localStorage.getItem("comic"));
  }

  getImage(thumbnail) {
    return `${thumbnail.path}.${thumbnail.extension}`;
  }

  deleteFavorites(idComic, index) {
    this.comics.splice(index, 1);
    localStorage.setItem("comic", JSON.stringify(this.comics))
  }
}
