import { ChangeDetectionStrategy, Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { SearchService } from '../../service/search.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public form: FormGroup;

  constructor(public search: SearchService) {
    this.form = new FormGroup({
      'search': new FormControl('')
    });

    this.form.valueChanges.subscribe((data: any) => {
      this.search.searchText(data.search);
    });

  }

  ngOnInit() {

  }

  searchCharacter(text: string) {
    this.search.searchText(text);
  }

}