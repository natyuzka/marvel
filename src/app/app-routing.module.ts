import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { ComicsComponent } from './component/comics/comics.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'characters/:id', component: ComicsComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
