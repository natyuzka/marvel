import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {

  }
  url = 'https://gateway.marvel.com/v1/public/';
  authorization = 'ts=3&apikey=af71efd3a009df55af8b5a35fb82d00e&hash=d8ce44a5512a3c36e22f25a88dfd783b';

  getComics() {
    return this.http.get( `${this.url}comics?${this.authorization}`);
  }
  getCharacters(searchText, order){
    if(searchText){
      return this.http.get( `${this.url}characters?nameStartsWith=${searchText}&limit=100&orderBy=${order}&${this.authorization}`); 
    }
    return this.http.get( `${this.url}characters?&orderBy=${order}&limit=100&${this.authorization}`);
  }
  getComicId(resourceUrl){
    return this.http.get( `${resourceUrl}?${this.authorization}`);
  }
  getFavourites(idComic){
    return this.http.get( `${this.url}comics?${idComic}${this.authorization}`);
  }
}
