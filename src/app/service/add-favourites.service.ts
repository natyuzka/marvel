import { Injectable, Output, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddFavouritesService {

  private comic: string;

  @Output() text: EventEmitter<string> = new EventEmitter();
  
  constructor() { }

  addFavourites(text: string){
    this.comic = text;
    this.text.emit(this.comic);
  }
}

