import { Injectable, Output, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  
  private dataText: string;

  @Output() text: EventEmitter<string> = new EventEmitter();
  
  constructor() { }

  searchText(text: string){
    this.dataText = text;
    this.text.emit(this.dataText);
  }
}
