import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularMaterialModule } from './material.module';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CardComponent} from './component/card/card.component';
import { AuthService } from './service/auth.service';
import { DialogAddCharacterComponent } from './component/dialog-add-character/dialog-add-character.component';
import { FavouritesComponent } from './component/favourites/favourites.component';

import { NgxPaginationModule} from 'ngx-pagination';
import { ComicsComponent } from './component/comics/comics.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CardComponent,
    DialogAddCharacterComponent,
    FavouritesComponent,
    ComicsComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AngularMaterialModule,
    NgxPaginationModule
    /*AngularFireModule.initializeApp(environment),
    AngularFireAuthModule*/
  ],
  entryComponents:[
    DialogAddCharacterComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
